ScalewayKube is a collection of resources that creates a fully functioning Kubernetes cluster from [10€/month](https://www.scaleway.com/pricing/).

It uses terraform, ansible and kubeadm tp create a cluster hosted on scaleway cloud, and also to change DNS records on OVH registrar.

## Table of Contents
- [Installation](#installation)
  * [Dependencies](#dependencies)
  * [DNS](#dns)
  * [Provisioning](#provisioning)
    + [Prerequisite](#prerequisite)
    + [Configuration](#terraform-configuration)
    + [Execute plan](#execute-plan)
  * [Kubernetes Setup](#kubernetes-setup)
    + [Configuration](#ansible-configuration)
        * [Hosts](#hosts)
        * [Variables](#variables)
    + [Setup kubernetes](#setup-kubernetes)
    + [Allow external access and install additional tools](#allow-external-access-and-install-additional-tools)
- [Usage](#usage)
    + [Obtain a ssl certificate](#obtain-a-ssl-certificate)
    + [Create a TLS secret](#create-a-tls-secret)
    + [Create a deployment](#create-a-deployment)
    + [Create a service](#create-a-service)
    + [Create an ingress to handle incoming traffic](#create-an-ingress-to-handle-incoming-traffic)
    + [Validating deployment](#validating-deployment)
- [FAQ](#faq)


# Installation
Pull the repository and move to the created folder:

```sh
git clone git@bitbucket.org:walligator/scalewaykube.git && cd scalewaykube
```

## Dependencies
 - Docker: [doc](https://docs.docker.com/install/)
 - Terraform: [doc](https://www.terraform.io/intro/getting-started/install.html)
 - Ansible: [doc](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## DNS
This repository assumes there is already a DNS zone created on OVH registrar, for which you obtained an application key and secret.

## Provisioning
### Prerequisite
Scaleway cloud requires a ssh public key to be setup on their [console](https://cloud.scaleway.com/#/credentials). Any servers launched on the account will get this ssh public key set on the remote root *authorized_keys* file, and the private counterpart will be used to ssh into those servers.

Before going to the next step, please ensure ssh is configured to access your servers with this key. You may do so by adding the following to your ssh config file (~/.ssh/config)

```sh
# ~/.ssh/config
Host *example.com
	User		       root
	IdentityFile   ~/.ssh/your_private_key_file
	ForwardAgent   yes
	IdentitiesOnly yes

```

### Terraform configuration
The cluster provisioning can be configured with 2 files:
- terraform/variables.tf
- terraform/terraform.tfvars

variables.tf contains configuration variables to customize the cluster provisioning.

terraform.tfvars contains private information such as API keys and secrets for the cloud/DNS providers. Do not version control this file.

Set your Scaleway Organization ID and API token, as well as your OVH application key, application secret and consumer key ([OVH documentation](https://docs.ovh.com/gb/en/customer/first-steps-with-ovh-api/)) in the terraform/terraform.tfvars file.

Review the default values in the terraform/variables.tf and change them according to your needs.

### Execute plan
Once you have edited these variables, you may provision your infrastructure by running the following commands.

Init terraform:
```sh
cd terraform
terraform init
```

Create an execution plan:
```sh
terraform plan
```

Execute the plan (You'll be asked by terraform to confirm the changes)
```sh
terraform apply
```

## Kubernetes Setup
Once the cloud resources have been successfully provisioned, you are ready to setup a Kubernetes cluster on the nodes.

For this step, Ansible will be used to execute commands remotely on those nodes. Therefore it is advised to review your ssh config file, as Ansible will use these information to attempt remote login.

### Ansible configuration
##### Hosts
Ansible requires a list of hosts, possibly grouped, to perform actions on.
These can be configured in host/groups file.

You can get more information as to how to write this file on Ansible documentation:
https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html

In the groups configuration provided, 4 groups are created (local, masters, nodes and cluster). The last group is grouping the 2 groups above it, which means all nodes in the cluster including masters.

##### Variables
Variables can be provided for each group defined above. To define them, a file matching the group name need to be created in *ansible/group_vars* folder.

In this example, we will be using the **cluster** group to set our variables.

### Setup kubernetes
To run the provided ansible playbook, please run the following command:

```sh
ansible-playbook site.yaml -i hosts/groups
```

Upon completion, a Kubernetes cluster should be up and running on your nodes. However, they are not accessible from your workstation yet.

### Allow external access and install additional tools
This will add an access from your workstation to the cluster, as well as starting up a few additional and common services (Distributed block storage, helm, ingress controller ...)

```sh
ansible-playbook local.yaml -i hosts/groups
```

# Usage
Your Kubernetes cluster should now be up and running, and ready to respond to incoming traffic. The below example shows how to create a tiny service that simply shows a single *"work-in-progress"* html page, running on Nginx.

This service willbe composed of the following elements:
- kube/test-nginx/secret.yaml
- kube/test-nginx/deployment.yaml
- kube/test-nginx/service.yaml
- kube/test-nginx/ingress.yaml

The deployment will be in charge of deploying the specified amount of nginx containers running in their respective pods. These will in turn be advertised by the service. The service will receive traffic routed by the ingress, which will also be using the secret to terminate the SSL connection.

### Obtain a ssl certificate
You can use let's encrypt to get a free ssl certificate. [Certbot](https://certbot.eff.org/docs/using.html#manual) would be a good candidate as it offers a DNS challenge method which will require a specific TXT subdomain and value to ensure you own the domain being verified. Once done, it will show you where the certificate and private key are located.

### Create a TLS secret
The certificate and key can now be deployed to the cluster as a secret.

You will need to set your base64 encoded certificate and private key respectively in tls.crt and tls.key properties.

First, base64 encode your certificate and key:
```sh
# Encode certificate
cat certs/walid.ovh/fullchain.pem | base64 -w 0

# Encode private key
cat certs/walid.ovh/privkey.pem | base64 -w 0
```

Then paste the values output from the two command above into kube/test-nginx/secret.yaml:
```yaml
apiVersion: v1
kind: Secret
metadata:
  name: walid.ovh
  namespace: walid
type: kubernetes.io/tls
data:
  tls.crt: [base64_encoded_certificate]
  tls.key: [base64_encoded_private_key]
```

Finally, submit this secret to kubernetes.
```sh
kubectl apply -f kube/test-nginx/secret.yaml
```

### Create a deployment
Submit the deployment file from kube/test-nginx/deployment.yaml:
```sh
kubectl apply -f kube/test-nginx/deployment.yaml
```

Note that you can create your own docker image, with the content and configuation you wish to serve from this service by following the documentation available here :
https://hub.docker.com/_/nginx/

### Create a service
Submit the service file from kube/test-nginx/deployment.yaml:
```sh
kubectl apply -f kube/test-nginx/deployment.yaml
```

This will advertise the deployment created at the step above on port 80.

### Create an ingress to handle incoming traffic
Next, we need to create an ingress to route incoming traffic to the service created in the previous step.
```sh
kubectl apply -f kube/test-nginx/ingress.yaml
```

### Validating deployment
In your browser, you should now see the service running at the URL you provided in the ingress.

![screenshot](screenshot.png)

# Scaling up
This project is relying on terraform to maintain the cluster state. To increase or dicrease the amount of nodes, follow these steps.

### Terraform
Simply change the count variable in the following files:
- terraform/variables.tf

```yaml
variable node_count {
  default = 3
}
```

Run terraform plan
```sh
cd terraform
terraform plan
```

This should show the changes to the cluster you are requesting.

Run terraform apply
```sh
terraform apply
```

### Ansible
Before running ansible playbook again, change the following variables.

Update your hosts in:
- ansible/group_vars/local

```yaml
---
# Servers
count: 3

```

Update your hosts in:
- ansible/development/hosts

Finally, run your ansible playbooks again.
```sh
cd ../ansible
ansible-playbook site.yaml -i development/hosts
ansible-playbook local.yaml -i development/hosts
```

# FAQ

1. **I can't provision persistentVolumes.**  
They are automatically provisioned. Please ensure your are specifying *storageClassName: rook-ceph-block* in your persistentvolumeclaim.  
1.
