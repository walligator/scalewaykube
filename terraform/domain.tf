provider "ovh" {
  endpoint           = "ovh-eu"
  application_key    = "${var.ovh_application_key}"
  application_secret = "${var.ovh_application_secret}"
  consumer_key       = "${var.ovh_consumer_key}"
}

############
## MASTER
############

resource "ovh_domain_zone_record" "master" {
    zone = "${var.dns_zone}"
    subdomain = "master.cluster"
    fieldtype = "A"
    ttl = "3600"
    target = "${scaleway_ip.master.ip}"
}

############
## NODES
############

resource "ovh_domain_zone_record" "nodes" {
    count = "${var.node_count}"
    zone = "${var.dns_zone}"
    subdomain = "node-${count.index}.cluster"
    fieldtype = "A"
    ttl = "3600"
    target = "${element(scaleway_ip.nodes.*.ip, count.index)}"
}

resource "ovh_domain_zone_record" "ingress" {
    zone = "${var.dns_zone}"
    subdomain = "cluster"
    fieldtype = "A"
    ttl = "3600"
    target = "${element(scaleway_ip.nodes.*.ip, 0)}"
}
