variable scw_organization {}

variable scw_api_token {}

variable ovh_application_key {}

variable ovh_application_secret {}

variable ovh_consumer_key {}

variable dns_zone {
  default = "walid.ovh"
}

variable owner {
  default = "cluster.walid.ovh"
}

variable region {
  default = "ams1"
}

variable node_count {
  default = 2
}
variable master_instance_type {
  default = "VC1S"
}

variable node_instance_type {
  default = "VC1M"
}

variable master_public_ip {
  default = "51.15.98.179"
}

variable node_public_ip {
  default = {
    "0" = "51.15.48.195"
    "1" = "51.15.109.194"
  }
}

variable image {
  default = "ab4d58a2-24c4-4f47-b364-4b9144b8fb4f"
}
