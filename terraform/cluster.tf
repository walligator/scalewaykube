provider "scaleway" {
  organization     = "${var.scw_organization}"
  token            = "${var.scw_api_token}"
  region           = "${var.region}"
}

############
## MASTER
############

# Create an IP for master
resource "scaleway_ip" "master" {}

# Create an instance for master
resource "scaleway_server" "master" {
  name             = "master.${var.owner}"
  image            = "${var.image}"
  type             = "${var.master_instance_type}"
  public_ip        = "${scaleway_ip.master.ip}"
  security_group = "${scaleway_security_group.masters.id}"
  tags             = ["KubernetesCluster", "master.${var.owner}"]
}

# Create security groups for masters
resource "scaleway_security_group" "masters" {
  name             = "masters.${var.owner}"
  description      = "Security group for Kubernetes masters"
}

# Create security group rules for masters
resource "scaleway_security_group_rule" "ssh_master" {
  security_group  = "${scaleway_security_group.masters.id}"

  action          = "accept"
  direction       = "inbound"
  ip_range        = "0.0.0.0/0"
  protocol        = "TCP"
  port            = 22
}

resource "scaleway_security_group_rule" "http_master" {
  security_group  = "${scaleway_security_group.masters.id}"

  action          = "accept"
  direction       = "inbound"
  ip_range        = "0.0.0.0/0"
  protocol        = "TCP"
  port            = 80
}

resource "scaleway_security_group_rule" "https_master" {
  security_group = "${scaleway_security_group.masters.id}"

  action          = "accept"
  direction       = "inbound"
  ip_range        = "0.0.0.0/0"
  protocol        = "TCP"
  port            = 443
}

resource "scaleway_security_group_rule" "api_server" {
  security_group  = "${scaleway_security_group.masters.id}"

  action          = "accept"
  direction       = "inbound"
  ip_range        = "0.0.0.0/0"
  protocol        = "TCP"
  port            = 6443
}

resource "scaleway_security_group_rule" "etcd_server" {
  security_group  = "${scaleway_security_group.masters.id}"

  action          = "accept"
  direction       = "inbound"
  ip_range        = "0.0.0.0/0"
  protocol        = "TCP"
  port            = 2379
}

resource "scaleway_security_group_rule" "etcd_server_api" {
  security_group  = "${scaleway_security_group.masters.id}"

  action          = "accept"
  direction       = "inbound"
  ip_range        = "0.0.0.0/0"
  protocol        = "TCP"
  port            = 2380
}

resource "scaleway_security_group_rule" "kubelet_api" {
  security_group  = "${scaleway_security_group.masters.id}"

  action          = "accept"
  direction       = "inbound"
  ip_range        = "0.0.0.0/0"
  protocol        = "TCP"
  port            = 10250
}

resource "scaleway_security_group_rule" "kube_scheduler" {
  security_group  = "${scaleway_security_group.masters.id}"

  action          = "accept"
  direction       = "inbound"
  ip_range        = "0.0.0.0/0"
  protocol        = "TCP"
  port            = 10251
}

resource "scaleway_security_group_rule" "kube_controller" {
  security_group  = "${scaleway_security_group.masters.id}"

  action          = "accept"
  direction       = "inbound"
  ip_range        = "0.0.0.0/0"
  protocol        = "TCP"
  port            = 10252
}

resource "scaleway_security_group_rule" "internal_traffic" {
  security_group  = "${scaleway_security_group.masters.id}"

  action          = "accept"
  direction       = "inbound"
  ip_range        = "10.8.0.0/16"
  protocol        = "TCP"
}

############
## Nodes
############

resource "scaleway_ip" "nodes" {
  count           = "${var.node_count}"
}

# Create an instance for master
resource "scaleway_server" "node" {
  count           = "${var.node_count}"
  name            = "node-${count.index}.${var.owner}"
  image           = "${var.image}"
  type            = "${var.node_instance_type}"
  public_ip       = "${element(scaleway_ip.nodes.*.ip, count.index)}"
  security_group  = "${scaleway_security_group.nodes.id}"
  tags            = ["KubernetesCluster", "node-${count.index}.${var.owner}"]

  volume {
    size_in_gb    = 50
    type          = "l_ssd"
  }
}

# Create security group for nodes
resource "scaleway_security_group" "nodes" {
  name            = "nodes.${var.owner}"
  description     = "Security group for Kubernetes nodes"
}

# Nodes security group rules
resource "scaleway_security_group_rule" "ssh_nodes" {
  security_group  = "${scaleway_security_group.nodes.id}"

  action          = "accept"
  direction       = "inbound"
  ip_range        = "0.0.0.0/0"
  protocol        = "TCP"
  port            = 22
}

resource "scaleway_security_group_rule" "kubelet_api_nodes" {
  security_group  = "${scaleway_security_group.nodes.id}"

  action          = "accept"
  direction       = "inbound"
  ip_range        = "0.0.0.0/0"
  protocol        = "TCP"
  port            = 10250
}

resource "scaleway_security_group_rule" "nodeport_services" {
  security_group  = "${scaleway_security_group.nodes.id}"

  action          = "accept"
  direction       = "inbound"
  ip_range        = "10.8.0.0/16"
  protocol        = "TCP"
}

resource "scaleway_security_group_rule" "http" {
  security_group  = "${scaleway_security_group.nodes.id}"

  action          = "accept"
  direction       = "inbound"
  ip_range        = "0.0.0.0/0"
  protocol        = "TCP"
  port            = 80
}

resource "scaleway_security_group_rule" "https" {
  security_group  = "${scaleway_security_group.nodes.id}"

  action          = "accept"
  direction       = "inbound"
  ip_range        = "0.0.0.0/0"
  protocol        = "TCP"
  port            = 443
}
