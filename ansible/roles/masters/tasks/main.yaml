# Run kubeadm init
- stat:
    path: /etc/kubernetes/admin.conf
  register: initiated

- name: Initialise master
  shell: kubeadm init --pod-network-cidr={{ kube_pod_cidr }} --skip-token-print
  register: results
  become: yes
  when: initiated.stat.exists == false

- name: Create kubeadm token
  shell: kubeadm token create
  register: kubeadm_token

- name: Get ca.cert hash
  shell: openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null |    openssl dgst -sha256 -hex | sed 's/^.* //'
  register: kubeadm_cacert_hash

- name: Get kubeadm host
  shell: kubeadm config view | grep advertiseAddress | awk '{match($0,/advertiseAddress:\s/); print $2}'
  register: kubeadm_host

- name: Get kubeadm port
  shell: kubeadm config view | grep bindPort | awk '{match($0,/bindPort:\s/); print $2}'
  register: kubeadm_port

- name: Save host variables
  set_fact:
    kubeadm_token: "{{ kubeadm_token.stdout }}"
    kubeadm_cacert_hash: "{{ kubeadm_cacert_hash.stdout }}"
    kubeadm_host: "{{ kubeadm_host.stdout }}"
    kubeadm_port: "{{ kubeadm_port.stdout }}"

# Store kubeconfig
- name: Ensure ~/.kube directory exists
  file:
    path: /home/{{ user }}/.kube
    state: directory
    owner: "{{ user }}"
    group: "{{ group }}"
    mode: 0750
  become: yes

- name: Copy kubeconfig to local user
  copy:
    src: /etc/kubernetes/admin.conf
    dest: /home/{{ user }}/.kube/config
    remote_src: yes
    owner: "{{ user }}"
    group: "{{ group }}"
    mode: 0700
  become: yes

- name: Set /proc/sys/net/bridge/bridge-nf-call-iptables to 1
  shell: sysctl net.bridge.bridge-nf-call-iptables=1
  become: yes

- name: Deploy pod network to the cluster
  shell: kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
  become: yes
  become_user: k8s

# Accessing cluster outside the master
- name: Copy the administrator kubeconfig file from your master to your workstation
  fetch:
    src: /etc/kubernetes/admin.conf
    dest: "{{ kubeconfig_path }}"
    flat: yes
